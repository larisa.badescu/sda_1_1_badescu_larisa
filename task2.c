#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Node 
{
    int size,state;
    char ID[10],memo[10],descrp[30];
    struct Node* next;
};

struct Node* createNode(char ID[],char memo[],char descrp[],int size,int state) 
{
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) 
    {
        printf("Malloc error\n");
    }
    newNode->size=size;
    newNode->state=state;
    strcpy(newNode->ID,ID);
    strcpy(newNode->memo,memo);
    strcpy(newNode->descrp,descrp);
    newNode->next=NULL;
    return newNode;
}

void insertHead(struct Node** head, char ID[],char memo[],char descrp[],int size,int state) 
{
    struct Node* newNode=createNode(ID,memo,descrp,size,state);
    newNode->next=*head;
    *head=newNode;
}

void print(struct Node* head)
{
    struct Node* current=head;
    while(current!=NULL) 
    {
        char status[5];
        if(current->state==1)
            strcpy(status,"Busy");
        else
            strcpy(status,"Free");

        printf("ID:%s & Status: %s\n",current->ID,status);
        current=current->next;
    }
}

void freeList(struct Node** head) 
{
    struct Node* current=*head;
    struct Node* nextNode;
    while (current!=NULL) 
    {
        nextNode=current->next;
        free(current);
        current=nextNode;
    }
    *head=NULL;
}

int checkingEmpty(struct Node* head) 
{
    if(head==NULL)
        return 1;
    return 0;
}

void freeNode(struct Node* list, char* ID) {
    struct Node* current= list;
    while (current!= NULL) {
        if (strcmp(current->ID, ID) == 0 && current->state == 1) {
            strcpy(current->memo,"");
            strcpy(current->descrp,"");
            current->size= 0;
            current->state= 0;
            return; 
        }
        current= current->next;
    }
    printf("Resource not found\n");
}

int main() 
{
    int size,stop=0,command,state;
    char ID[20],descrp[40],memo[20];
    struct Node* head=NULL;
    FILE *fin=fopen("exemples.txt","r");
    if(fin==NULL)
    {
        printf("Error opening the file"); 
    }

    while(stop==0)
    {
        while(fscanf(fin,"%[^,],%[^,],\"%50[^\"]\",%d,%d",ID,memo,descrp,&size,&state)==5)   
        {  
            insertHead(&head,ID,memo,descrp,size,state);         
        }
        printf("\n         Menu :)       \n");
        printf("\nWrite 1 to free a received ID\nWrite 2 to display the current status of all the IDs\nWrite 3 to free all the resources\nWrite 0 to exit the program\n");
        
        scanf("%d",&command);
        if(command==0)
            {
                stop=1;
            }
        if(command==1)
            {
                char aux[10];
                scanf("%s",aux);
                freeNode(head,aux);
            }
        if(command==2)
            {
                printf("\n           List Status:       \n\n");
                print(head);
            }
        if(command==3)
            {
                printf("\n        The list is free        \n\n");
                freeList(&head);
            }
    }
}