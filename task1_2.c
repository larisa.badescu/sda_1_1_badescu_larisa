#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

char search[]="watch";

typedef struct        //Struct for each word
{
    char word[20];
    int occurence;
    double freq;
}Word;

Word array[1000];

void swap(Word *i,Word *j){
    Word aux=*i;
    *i=*j;
    *j=aux;
}

int partition(Word a[], int left,int right)  //Quicksort
{
    int sw=a[right].occurence;
    int i=(left-1);

    for (int j=left;j<=right-1;j++) 
    {
        if (a[j].occurence>=sw) 
        {
            i++;
            swap(&a[i],&a[j]);
        }
    }
    swap(&a[i+1],&a[right]);
    return (i+1);
}

void quicksort(Word a[], int left, int right) 
{
    if (left < right) 
    {
        int swi = partition(a, left, right);
        quicksort(a, left, swi - 1);
        quicksort(a, swi + 1, left);
    }
}

int b_search(Word array[], int l, int r, char *s)      //binary search
{
    while(l<=r)
    {
        int m=l+(r-l)/2;

        if(strcmp(array[m].word,s)==0)
        return m;
        if(strcmp(array[m].word,s)<0)
        r=m-1;
        else
        l=m+1;
    }
    return -1;
}

int main()
{
    FILE *fin;
    FILE *fout;
    FILE *fsearch;    
    FILE *osearch;

    fin=fopen("words.csv" , "r");
    if(fin==NULL)
    {
        printf("Error fin\n");
        return -1;
    }
    fsearch=fopen("sorted.csv" , "r");
    if(fsearch==NULL)
    {
        printf("Error fsearch\n");
        return -1;
    }

    fout=fopen("output.txt" , "w+");
    if(fout==NULL)
    {
        printf("Error fout\n");
        return -1;
    }
    osearch=fopen("output_binary.txt" , "w+");
    if(osearch==NULL)
    {
        printf("Error osearch\n");
        return -1;
    }

    int cont=0;
    while(cont<1000 && fscanf(fin,"%19[^,],%d,%lf",array[cont].word,&array[cont].occurence,&array[cont].freq)==3)
    {
        cont++;
    }

    clock_t starttime1=clock();
    quicksort(array, 0, cont-1);                         //for sorting occurences descending
    clock_t endtime1=clock();
    double time1=(double)(endtime1-starttime1)/CLOCKS_PER_SEC;
    fprintf(osearch, "CLOCKTIME(quicksort): %lf", time1);

    for(int i=0;i<cont;i++) 
    {
        fprintf(fout, "%s,%d,%.2lf\n", array[i].word, array[i].occurence, array[i].freq);
    }
    fclose(fin);
    fclose(fout);

    int cont1=0;
    char auxl[3],auxr[3], copy[3];   
    int l,r;            
    while(cont1<1000 && fscanf(fsearch,"%19[^,],%d,%lf",array[cont1].word,&array[cont1].occurence,&array[cont1].freq)==3)
    {
        cont1++;
    }

    strncpy(copy,search,1);
    for(int i=0;i<cont1;i++)          //for finding L
    {
        strncpy(auxl,array[i].word,1);
        if(strcmp(auxl,copy)==0)
        {
            l=i;
            break;
        }
    }
        if(!(l>=1 && l<=1000))
        fprintf(osearch,"error L\n");
    
    for(int i=cont1;i>0;i++)             //for finding R
    {
        strncpy(auxr,array[i].word,1);
        if(strcmp(auxr,copy)==0)
        {
            r=i;
            break;
        }
    }
    if(!(r>=1 && r<=1000))
    fprintf(osearch,"error R\n");

    clock_t starttime=clock();
    int bsearch=b_search(array,l,r-1,search);
    clock_t endtime=clock();
    fprintf(osearch,"%d\n",bsearch);
    double time=(double)(endtime-starttime)/CLOCKS_PER_SEC;
    fprintf(osearch, "CLOCKTIME: %lf", time);

    fclose(fsearch);
    fclose(osearch);
    return 0;
}